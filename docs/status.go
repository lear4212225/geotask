package docs

import "gitlab.com/ptflp/geotask/module/courierfacade/models"

// добавить документацию для роута /api/status

// swagger:route GET /api/status status statusRequest
// Получение статуса сервиса.
// responses:
//			200: statusResponse

// swagger:parameters statusRequest
type statusRequest struct {
}

// swagger:response statusResponse
type statusResponse struct {
	// in:body
	Body models.CourierStatus
}
