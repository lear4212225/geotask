package service

import (
	"context"
	cservice "gitlab.com/ptflp/geotask/module/courier/service"
	cfm "gitlab.com/ptflp/geotask/module/courierfacade/models"
	oservice "gitlab.com/ptflp/geotask/module/order/service"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) error // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) (cfm.CourierStatus, error)   // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func (c CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) error {
	cour, err := c.courierService.GetCourier(ctx)
	if err != nil {
		return err
	}
	err = c.courierService.MoveCourier(*cour, direction, zoom)
	if err != nil {
		return err
	}
	return nil
}

func (c CourierFacade) GetStatus(ctx context.Context) (cfm.CourierStatus, error) {
	var status cfm.CourierStatus
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		return cfm.CourierStatus{}, err
	}
	status.Courier = *courier
	orderByRadius, err := c.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "m")
	if err != nil {
		return cfm.CourierStatus{}, err
	}
	status.Orders = orderByRadius

	return status, nil
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}
