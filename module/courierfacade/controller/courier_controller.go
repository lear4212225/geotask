package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/ptflp/geotask/module/courierfacade/service"
	"log"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)

	// получить статус курьера из сервиса courierService используя метод GetStatus
	status, err := c.courierService.GetStatus(ctx)
	if err != nil {
		log.Println(err)
	}
	// отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	//	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	data, ok := m.Data.([]uint8)
	if !ok {
		log.Println("error move Courier")
		return
	}
	err := json.Unmarshal(data, &cm)
	if err != nil {
		log.Println("error unmarshal")
		return
	}

	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
