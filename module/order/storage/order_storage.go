package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/ptflp/geotask/module/order/models"
	"strconv"
	"time"
)

type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *redis.Client
}

func NewOrderStorage(storage *redis.Client) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	// получить ID всех старых ордеров, которые нужно удалить
	// используя метод ZRangeByScore
	// старые ордеры это те, которые были созданы две минуты назад
	// и более
	/**
	&redis.ZRangeBy{
		Max: использовать вычисление времени с помощью maxAge,
		Min: "0",
	}
	*/
	// Проверить количество старых ордеров
	maxTime := fmt.Sprintf("%d", time.Now().UnixNano()-int64(maxAge))
	res, err := o.storage.ZRangeByScore(ctx, "order", &redis.ZRangeBy{
		Max: maxTime,
		Min: "0",
	}).Result()
	if err != nil {
		return err
	}

	if len(res) == 0 {
		return nil
	}

	// удалить старые ордеры из redis используя метод ZRemRangeByScore где ключ "orders" min "-inf" max "(время создания старого ордера)"
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни
	err = o.storage.ZRangeByScore(ctx, "orders", &redis.ZRangeBy{
		Max: maxTime,
		Min: "-inf",
	}).Err()
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var order models.Order
	// получаем ордер из redis по ключу order:ID
	cmd := o.storage.Get(ctx, "order:"+strconv.Itoa(orderID))
	if cmd.Err() != nil {
		return nil, cmd.Err()
	}
	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil
	err = cmd.Scan(order)
	if err != nil {
		return nil, err
	}

	// десериализуем ордер из json +

	return &order, nil
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)
	// сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge
	key := "order:" + strconv.Itoa(int(order.ID))
	err = o.storage.Set(ctx, key, data, maxAge).Err()
	if err != nil {
		return err
	}

	// добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты
	err = o.storage.GeoAdd(ctx, "location",
		&redis.GeoLocation{
			Name:      key,
			Latitude:  order.Lat,
			Longitude: order.Lng,
		},
	).Err()
	if err != nil {
		return err
	}

	// zset сохраняем ордер для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.ZAdd(ctx, "orders",
		redis.Z{Score: float64(time.Now().UnixNano()), Member: key}).Err()
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard(ctx, "orders").Result()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var orders []models.Order
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	orders = make([]models.Order, 0, len(ordersLocation))
	// проходим по списку ID заказов и получаем данные о заказе
	// получаем данные о заказе по ID из redis по ключу order:ID
	for _, elem := range ordersLocation {
		order := models.Order{}
		data, err := o.storage.Get(ctx, elem.Name).Bytes()
		if err == redis.Nil {
			return nil, err
		}
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal(data, &order)
		if err != nil {
			return nil, err
		}

		orders = append(orders, order)
	}

	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки
	/**
	&redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	*/
	places, err := o.storage.GeoRadius(ctx, "location", lng, lat,
		&redis.GeoRadiusQuery{
			Radius:      radius,
			Unit:        unit,
			WithCoord:   true,
			WithDist:    true,
			WithGeoHash: true,
		}).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}

	return places, nil

}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {

	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	var err error
	var id int64
	id, err = o.storage.Incr(ctx, "order:id").Result()
	if err != nil {
		return 0, err
	}
	return id, nil
}
