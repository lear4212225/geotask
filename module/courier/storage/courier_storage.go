package storage

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"gitlab.com/ptflp/geotask/module/courier/models"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	courierJson, err := json.Marshal(courier)
	if err != nil {
		return err
	}

	err = c.storage.Set(ctx, "courier", courierJson, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	courier := models.Courier{}
	data, err := c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
