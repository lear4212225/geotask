package service

import (
	"context"
	"errors"
	"github.com/redis/go-redis/v9"
	"gitlab.com/ptflp/geotask/geo"
	"gitlab.com/ptflp/geotask/module/courier/models"
	"gitlab.com/ptflp/geotask/module/courier/storage"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	cour, err := c.courierStorage.GetOne(ctx)
	if err == redis.Nil {
		courier := models.Courier{
			Score: 0,
			Location: &models.Point{
				Lat: DefaultCourierLat,
				Lng: DefaultCourierLng,
			},
		}
		err = c.courierStorage.Save(ctx, courier)
		if err != nil {
			return nil, err
		}

	} else if err != nil {
		return nil, err
	}

	// проверяем, что курьер находится в разрешенной зоне
	p := geo.Point{
		Lat: cour.Location.Lat,
		Lng: cour.Location.Lng,
	}

	if !c.allowedZone.Contains(p) {
		// если нет, то перемещаем его в случайную точку в разрешенной зоне
		newPoint := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		cour.Location = &models.Point{
			Lat: newPoint.Lat,
			Lng: newPoint.Lng,
		}
		// сохраняем новые координаты курьера
		err = c.courierStorage.Save(ctx, *cour)
		if err != nil {
			return nil, err
		}

	}

	return cour, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	form := 0.001 / math.Pow(2, float64(zoom-14))
	switch direction {
	case DirectionUp:
		courier.Location.Lat += form
	case DirectionDown:
		courier.Location.Lat -= form
	case DirectionLeft:
		courier.Location.Lng -= form
	case DirectionRight:
		courier.Location.Lng += form
	default:
		return errors.New("direction not avalible")

	}

	// далее нужно проверить, что курьер не вышел за границы зоны
	var newLocationCourier *models.Point
	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		// если вышел, то нужно переместить его в случайную точку внутри зоны
		tempPoint := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		newLocationCourier = &models.Point{
			Lat: tempPoint.Lat,
			Lng: tempPoint.Lng,
		}
		courier.Location = newLocationCourier
	}

	// далее сохранить изменения в хранилище

	// сохраняем новые координаты курьера
	err := c.courierStorage.Save(context.Background(), courier)
	if err != nil {
		return err
	}
	return nil
}
