package geo

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	// проверить, находится ли точка в разрешенной зоне
	if !allowedZone.Contains(point) {
		return false
	}
	for _, zone := range disabledZones {
		if zone.Contains(point) {
			return false
		}
	}
	return true
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var point Point
	// получение случайной точки в разрешенной зоны
	noAllowed := true
	for noAllowed {
		point = allowedZone.RandomPoint()
		if CheckPointIsAllowed(point, allowedZone, disabledZones) {
			noAllowed = false
		}
	}

	return point
}

func NewDisAllowedZone1() (*Polygon, error) {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	var targetPolygons = "noOrdersPolygon1"
	points, err := getPolygons(targetPolygons)
	if err != nil {
		return nil, err
	}

	return NewPolygon(points, false), err
}

func NewDisAllowedZone2() (*Polygon, error) {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	var targetPolygons = "noOrdersPolygon2"
	points, err := getPolygons(targetPolygons)
	if err != nil {
		return nil, err
	}

	return NewPolygon(points, false), nil
}

func NewAllowedZone() (*Polygon, error) {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	var targetPolygons = "mainPolygon"
	points, err := getPolygons(targetPolygons)
	if err != nil {
		return nil, err
	}

	return NewPolygon(points, true), nil
}
