package geo

import (
	geo "github.com/kellydunn/golang-geo"
	"math/rand"
)

const pathPolygons = "./public/js/polygon.js"

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	linkPoints := make([]*geo.Point, 0, len(points))
	for _, point := range points {
		linkPoints = append(linkPoints, geo.NewPoint(point.Lat, point.Lng))
	}

	return &Polygon{
		polygon: geo.NewPolygon(linkPoints),
		allowed: allowed,
	}
}

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

func (p *Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}

func (p *Polygon) RandomPoint() Point {
	var (
		minLat float64 = 180
		maxLat float64 = -180
		minLng float64 = 180
		maxLng float64 = -180
	)

	points := p.polygon.Points()
	for _, point := range points {
		if point.Lat() > maxLat {
			maxLat = point.Lat()
		}
		if point.Lng() > maxLng {
			maxLng = point.Lng()
		}
		if point.Lat() < minLat {
			minLat = point.Lat()
		}
		if point.Lng() < minLng {
			minLng = point.Lng()
		}
	}

	lat := minLat + rand.Float64()*(maxLat-minLat)
	lng := minLng + rand.Float64()*(maxLng-minLng)

	point := Point{
		Lat: lat,
		Lng: lng,
	}

	return point
}
