package geo

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func getPolygons(targetPolygons string) ([]Point, error) {
	file, err := os.Open(pathPolygons)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	points := make([]Point, 0, 10)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, targetPolygons) {
			for scanner.Scan() {
				line := scanner.Text()
				if strings.Contains(line, "];") {
					break
				}
				line = strings.Replace(line, "[", "", 1)
				line = strings.Replace(line, "]", "", 1)
				line = strings.Replace(line, ",", "", 2)
				line = strings.TrimSpace(line)
				coords := strings.Split(line, " ")
				coord1, err := strconv.ParseFloat(coords[0], 64)
				if err != nil {
					return nil, err
				}
				coord2, err := strconv.ParseFloat(coords[1], 64)
				if err != nil {
					return nil, err
				}
				points = append(points, Point{
					Lat: coord1,
					Lng: coord2,
				})
			}
		}
	}
	return points, err
}
